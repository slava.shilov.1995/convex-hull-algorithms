#pragma once
#include "GlobalData.h"

#include <vector>

namespace gen {
	enum DistributionType {Uniform, Circle, Edge, Ring };
	extern std::vector<std::string> distributionTypeTitle;
	extern DistributionType distributionType;
	
	void GenerateData(std::vector<glob::Point>& data, bool verbose);
	void SetDistributionType(gen::DistributionType type);
	std::vector<int> LinspaceInt(int start, int end, int steps);
}