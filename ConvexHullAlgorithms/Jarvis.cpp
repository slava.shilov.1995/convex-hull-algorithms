#include "Jarvis.h"

namespace jrv {

	std::vector<glob::Point>& data = glob::p;
	glob::Point startPoint;

	std::vector<int> Jarvis(std::vector<glob::Point>& p) {
		data = p;
		std::vector<int> ch;

		int target = 0;
		for (int i = 1; i < data.size(); ++i) {
			if (data[i].x < data[target].x) {
				target = i;
			} else if (data[i].x == data[target].x) {
				if (data[i].y > data[target].y) {
					target = i;
				}
			}
		}
		std::swap(data[target], data[data.size() - 1]);
		startPoint = data[data.size() - 1];
		
		double c;
		int l = data.size() - 1;
		target = 0;
		while (true) {
			for (int i = 0; i < l; ++i) {
				c = (data[target].x - data[l].x) * (data[i].y - data[l].y) - (data[target].y - data[l].y) * (data[i].x - data[l].x);
				if (c >= 0) {
					if (c == 0) {
						if ((data[target].x - data[l].x) * (data[i].x - data[target].x) + (data[target].y - data[l].y) * (data[i].y - data[target].y) > 0) {
							target = i;
						}
					} else {
						target = i;
					}
				}
			}
			c = (data[target].x - data[l].x) * (data[data.size() - 1].y - data[l].y) - (data[target].y - data[l].y) * (data[data.size() - 1].x - data[l].x);
			if (c >= 0) {
				if (c == 0) {
					if ((data[target].x - data[l].x) * (data[data.size() - 1].x - data[target].x) + (data[target].y - data[l].y) * (data[data.size() - 1].y - data[target].y) > 0) {
						break;
					}
				} else {
					break;
				}
				//break;
			} 
			std::swap(data[--l], data[target]);
			if (l <= 0) {
				break;
			}
			target = 0;
		}
		
		for (int i = data.size() - 1; i >= l; --i) {
			//ch.emplace_back(data[i].id);
			ch.emplace_back(i);
		}

		return ch;
	}
}