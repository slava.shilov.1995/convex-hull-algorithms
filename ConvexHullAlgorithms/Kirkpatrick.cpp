#include "Kirkpatrick.h"

#include <algorithm>
#include <iostream>

namespace krk {

	std::vector<glob::Point>& data = glob::p;
	std::vector<int> ch;

	double precision = 0.0000000001;

	struct Pair {
		glob::Point pi;
		glob::Point pj;
		double slope;

		Pair(glob::Point& pi, glob::Point& pj) : pi(pi), pj(pj) {
			slope = (pi.y - pj.y) / (pi.x - pj.x);
		};

		bool operator<(Pair& r) {
			return slope < r.slope;
		}
	};

	std::pair<int, int> BridgeU(std::vector<glob::Point> S, double a) {
		while (true) {
			if (S.size() == 2) {
				if (S[0] < S[1]) {
					return std::pair<int, int>(S[0].id, S[1].id);
				} else {
					return std::pair<int, int>(S[1].id, S[0].id);
				}
			}
			std::vector<glob::Point> candidates;
			std::vector<Pair> pairs;
			int j;
			for (int i = 0; i < S.size() - 1; i += 2) {
				j = i + 1;
				if (S[i].x == S[j].x) {
					if (S[i].y > S[j].y) {
						candidates.emplace_back(S[i]);
					} else {
						candidates.emplace_back(S[j]);
					}
				} else {
					if (S[i].x < S[j].x) {
						pairs.emplace_back(S[i], S[j]);
					} else {
						pairs.emplace_back(S[j], S[i]);
					}
				}
			}
			if (!pairs.empty()) {
				std::nth_element(pairs.begin(), pairs.begin() + pairs.size() / 2, pairs.end());
				double K = pairs[pairs.size() / 2].slope;;
				/*std::vector<Pair> small, equal, large;
				for (Pair pair : pairs) {
					if (pair.slope < K) {
						small.emplace_back(pair);
					} else if (pair.slope > K) {
						large.emplace_back(pair);
					} else {
						equal.emplace_back(pair);
					}
				}*/
				std::vector<glob::Point> max;
				double maximum = S[0].y - K * S[0].x;
				double c;
				for (glob::Point pi : S) {
					c = pi.y - K * pi.x;
					if (c - maximum > precision) {
						maximum = c;
						max.clear();
						max.emplace_back(pi);
					} else if (abs(c - maximum) < precision) {
						max.emplace_back(pi);
					}
				}
				glob::Point pk = max[0], pm = max[0];
				for (glob::Point pi : max) {
					if (pi.x < pk.x) {
						pk = pi;
					}
					if (pi.x > pm.x) {
						pm = pi;
					}
				}
				if (pk.x <= a && pm.x >= a) {
					return { pk.id, pm.id };
				}
				if (pm.x <= a) {
					for (Pair pair : pairs) {
						candidates.emplace_back(pair.pj);
						if (pair.slope < K) {
							candidates.emplace_back(pair.pi);
						}
					}
					/*for (Pair pair : large) {
						candidates.emplace_back(pair.pj);
					}
					for (Pair pair : equal) {
						candidates.emplace_back(pair.pj);
					}
					for (Pair pair : small) {
						candidates.emplace_back(pair.pi);
						candidates.emplace_back(pair.pj);
					}*/
				}
				if (pk.x > a) {
					for (Pair pair : pairs) {
						candidates.emplace_back(pair.pi);
						if (pair.slope > K) {
							candidates.emplace_back(pair.pj);
						}
					}
					/*for (Pair pair : small) {
						candidates.emplace_back(pair.pi);
					}
					for (Pair pair : equal) {
						candidates.emplace_back(pair.pi);
					}
					for (Pair pair : large) {
						candidates.emplace_back(pair.pi);
						candidates.emplace_back(pair.pj);
					}*/
				}
			}
			if (S.size() % 2 == 1) {
				candidates.emplace_back(S[S.size() - 1]);
			}
			S = candidates;
		}
	}

	std::pair<int, int> BridgeD(std::vector<glob::Point> S, double a) {
		while (true) {
			if (S.size() == 2) {
				if (S[0] < S[1]) {
					return { S[0].id, S[1].id };
				} else {
					return { S[1].id, S[0].id };
				}
			}
			std::vector<glob::Point> candidates;
			std::vector<Pair> pairs;
			int j;
			for (int i = 0; i < S.size() - 1; i += 2) {
				j = i + 1;
				if (S[i].x == S[j].x) {
					if (S[i].y < S[j].y) {
						candidates.emplace_back(S[i]);
					} else {
						candidates.emplace_back(S[j]);
					}
				} else {
					if (S[i].x < S[j].x) {
						pairs.emplace_back(S[i], S[j]);
					} else {
						pairs.emplace_back(S[j], S[i]);
					}
				}
			}
			if (!pairs.empty()) {
				std::nth_element(pairs.begin(), pairs.begin() + pairs.size() / 2, pairs.end());
				double K = pairs[pairs.size() / 2].slope;;
				/*std::vector<Pair> small, equal, large;
				for (Pair pair : pairs) {
					if (pair.slope > K) {
						small.emplace_back(pair);
					} else if (pair.slope < K) {
						large.emplace_back(pair);
					} else {
						equal.emplace_back(pair);
					}
				}*/
				std::vector<glob::Point> max;
				double maximum = -(S[0].y - K * S[0].x);
				double c;
				for (glob::Point pi : S) {
					c = -(pi.y - K * pi.x);
					if (c - maximum > precision) {
						maximum = c;
						max.clear();
						max.emplace_back(pi);
					} else if (abs(c - maximum) < precision) {
						max.emplace_back(pi);
					}
				}
				glob::Point pk = max[0], pm = max[0];
				for (glob::Point pi : max) {
					if (pi.x < pk.x) {
						pk = pi;
					}
					if (pi.x > pm.x) {
						pm = pi;
					}
				}
				if (pk.x <= a && pm.x >= a) {
					return { pk.id, pm.id };
				}
				if (pm.x <= a) {
					for (Pair pair : pairs) {
						candidates.emplace_back(pair.pj);
						if (pair.slope > K) {
							candidates.emplace_back(pair.pi);
						}
					}
					/*for (Pair pair : large) {
						candidates.emplace_back(pair.pj);
					}
					for (Pair pair : equal) {
						candidates.emplace_back(pair.pj);
					}
					for (Pair pair : small) {
						candidates.emplace_back(pair.pi);
						candidates.emplace_back(pair.pj);
					}*/
				}
				if (pk.x > a) {
					for (Pair pair : pairs) {
						candidates.emplace_back(pair.pi);
						if (pair.slope < K) {
							candidates.emplace_back(pair.pj);
						}
					}
					/*for (Pair pair : small) {
						candidates.emplace_back(pair.pi);
					}
					for (Pair pair : equal) {
						candidates.emplace_back(pair.pi);
					}
					for (Pair pair : large) {
						candidates.emplace_back(pair.pi);
						candidates.emplace_back(pair.pj);
					}*/
				}
			}
			if (S.size() % 2 == 1) {
				candidates.emplace_back(S[S.size() - 1]);
			}
			S = candidates;
		}
	}

	void ConnectU(int k, int m, std::vector<glob::Point>& S) {
		std::nth_element(S.begin(), S.begin() + S.size() / 2, S.end());
		double a = S[S.size() / 2].x;
		int i, j;
		std::pair<int, int> ij = BridgeU(S, a);
		i = ij.first;
		j = ij.second;
		std::vector <glob::Point > Sl, Sr;
		Sl.emplace_back(data[i]);
		Sr.emplace_back(data[j]);
		for (int t = 0; t < S.size(); ++t) {
			if (S[t].x < data[i].x) {
				Sl.emplace_back(S[t]);
			} else if (S[t].x > data[j].x) {
				Sr.emplace_back(S[t]);
			}
		}
		if (i == k) {
			if (ch.back() != i) {
				ch.emplace_back(i);
			}
		} else {
			ConnectU(k, i, Sl);
		}
		if (j == m) {
			if (ch.back() != j) {
				ch.emplace_back(j);
			}
		} else {
			ConnectU(j, m, Sr);
		}
	}

	void ConnectD(int k, int m, std::vector<glob::Point>& S) {
		std::nth_element(S.begin(), S.begin() + S.size() / 2, S.end());
		double a = S[S.size() / 2].x;
		int i, j;
		std::pair<int, int> ij = BridgeD(S, a);
		i = ij.first;
		j = ij.second;
		std::vector <glob::Point > Sl, Sr;
		Sl.emplace_back(data[i]);
		Sr.emplace_back(data[j]);
		for (int t = 0; t < S.size(); ++t) {
			if (S[t].x < data[i].x) {
				Sl.emplace_back(S[t]);
			} else if (S[t].x > data[j].x) {
				Sr.emplace_back(S[t]);
			}
		}
		if (j == m) {
			if (ch.back() != j) {
				ch.emplace_back(j);
			}
		} else {
			ConnectD(j, m, Sr);
		}
		if (i == k) {
			if (ch.back() != i) {
				ch.emplace_back(i);
			}
		} else {
			ConnectD(k, i, Sl);
		}
	}

	std::vector<int> Kirkpatrick(std::vector<glob::Point>& points) {
		data = points;
		ch.clear();

		int mnu = 0, mxu = 0, mnd = 0, mxd = 0;
		for (int i = 0; i < data.size(); ++i) {
			if (data[i].x < data[mnu].x || (data[i].x == data[mnu].x && data[i].y > data[mnu].y)) {
				mnu = i;
			}
			if (data[i].x < data[mnd].x || (data[i].x == data[mnd].x && data[i].y < data[mnd].y)) {
				mnd = i;
			}
			if (data[i].x > data[mxu].x || (data[i].x == data[mxu].x && data[i].y > data[mxu].y)) {
				mxu = i;
			}
			if (data[i].x > data[mxd].x || (data[i].x == data[mxd].x && data[i].y < data[mxd].y)) {
				mxd = i;
			}
		}
		ch.push_back(mnu);
		if (mnu != mxu) {
			std::vector<glob::Point> T = { data[mnu], data[mxu] };
			for (int i = 0; i < data.size(); ++i) {
				if (data[mnu].x < data[i].x && data[i].x < data[mxu].x) {
					T.emplace_back(data[i]);
				}
			}
			ConnectU(mnu, mxu, T);
		}
		if (mnd != mxd) {
			std::vector<glob::Point> T = { data[mnd], data[mxd] };
			for (int i = 0; i < data.size(); ++i) {
				if (data[mnd].x < data[i].x && data[i].x < data[mxd].x) {
					T.emplace_back(data[i]);
				}
			}
			ConnectD(mnd, mxd, T);
		}

		if (ch.front() == ch.back()) {
			ch.pop_back();
		}

		/*for (int i : ch) {
			std::cout << i << " ";
		}
		std::cout << std::endl;*/

		return ch;
	}
}