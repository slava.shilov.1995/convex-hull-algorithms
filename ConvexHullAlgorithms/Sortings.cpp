#pragma once

#include "Sortings.h"

namespace srt {

	std::vector<glob::Point>& data = glob::p;

	//quicksort

	int orderA, _;
	int _i, _j;

	int Partition(int low, int high) {
		/*glob::Point p;
		int mid = (low + high) / 2;
		if ((data[low].cmpValue > data[mid].cmpValue) != (data[low].cmpValue > data[high].cmpValue)) {
			p = data[low];
		} else if ((data[mid].cmpValue > data[low].cmpValue) != (data[mid].cmpValue > data[high].cmpValue)) {
			p = data[mid];
		} else {
			p = data[high];
		}*/
		glob::Point p = data[(low + high) / 2.];
		//glob::Point p = data[(low + high) * 0.5];
		_i = low - 1;
		_j = high + 1;
		while (true) {
			do {
				orderA = ++_i;
			} while (data[orderA] << p);
			do {
				orderA = --_j;
			} while (p << data[orderA]);

			if (_i >= _j) {
				return _j;
			}

			std::swap(data[_i], data[_j]);
		}
	}

	void Quicksort(int low, int high) {
		if (low < high) {
			int p = Partition(low, high);
			Quicksort(low, p);
			Quicksort(p + 1, high);
		}
	}



	//max heap sort

	int n, m;
	int mid, left, right;

	void MaxHeapify(int i, int low) {
		left = 2 * (i + 1) - 1 + low;
		right = 2 * (i + 1) + low;
		mid = i + low;

		if ((left < low + n + 1) && data[mid] << data[left]) {
			m = left;
		} else {
			m = mid;
		}

		if ((right < low + n + 1) && data[m] << data[right]) {
			m = right;
		}

		if (m != mid) {
			std::swap(data[mid], data[m]);

			MaxHeapify(m - low, low);
		}
	}

	void BuildMaxHeap(int low, int high) {
		n = high - low;
		for (int i = n / 2.; i >= 0; --i) {
			MaxHeapify(i, low);
		}
	}

	void MaxHeapSort(int low, int high) {
		BuildMaxHeap(low, high);
		for (int i = low + n; i >= low; --i) {
			std::swap(data[low], data[i]);
			--n;
			MaxHeapify(0, low);
		}
	}

	//insertion sort

	void Insertionsort(int low, int high) {
		glob::Point key;
		int i;
		for (int j = low + 1; j <= high; ++j) {
			key = data[j];
			i = j - 1;
			while (i >= 0 && key << data[i]) {
				data[i + 1] = data[i];
				--i;
			}
			data[i + 1] = key;
		}
	}

	//introsort

	void Introsort(int low, int high, int level) {
		int n = high - low;
		if (n < 50) {
			Insertionsort(low, high);
		} else if (level == 0) {
			MaxHeapSort(low, high);
		} else {
			int p = Partition(low, high);
			Introsort(low, p, level - 1);
			Introsort(p + 1, high, level - 1);
		}
	}

	void Introsort(int low, int high) {
		Introsort(low, high, log(high - low + 1) * 2);
	}

	void SetDataReference(std::vector<glob::Point>& p) {
		data = p;
	}

}