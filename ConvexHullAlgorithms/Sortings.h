#pragma once

#include "GlobalData.h"

namespace srt {
	int Partition(int low, int high);
	void Quicksort(int low, int high);
	void MaxHeapSort(int low, int high);
	void Insertionsort(int low, int high);
	void Introsort(int low, int high, int level);
	void Introsort(int low, int high);
	void SetDataReference(std::vector<glob::Point>& p);

}