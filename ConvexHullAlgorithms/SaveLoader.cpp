#pragma once

#include "SaveLoader.h"
#include "GlobalData.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>



typedef std::numeric_limits< double > dbl;

namespace sl {
	double x, y;

	std::string loadFile = "data.txt";
	std::string saveFile = "save.txt";
	std::string statsFile = "stats.csv";

	void Save() {
		std::ofstream file(saveFile);
		if (file) {
			file.setf(std::ios::fixed);
			file.precision(dbl::max_digits10);
			file << glob::N << std::endl;

			for (int i = 0; i < glob::N; ++i) {
				file << glob::data[i].x << " " << glob::data[i].y << std::endl;
			}
			file.close();
			std::cout << "Points set saved into " << saveFile << std::endl;
		} else {
			std::cout << "Error: could not save data into " << saveFile << std::endl;
		}
	}

	bool Load() {
		std::ifstream file(loadFile);
		if (file) {
			std::string line;
			std::getline(file, line);
			std::istringstream iss(line);
			iss >> glob::N;
			glob::data.resize(glob::N);
			for (int i = 0; i < glob::N; ++i) {
				std::getline(file, line);
				iss = std::istringstream(line);
				iss >> x >> y;
				glob::data[i] = glob::Point(x, y, i);
			}
			file.close();
		} else {
			std::cout << "Error: could not read file " << loadFile << std::endl;
			return false;
		}
		return true;
	}

	void SaveStats(std::vector<std::vector<glob::Point>> stats) {
		std::string div = "\t";
		std::string endl = "\n";

		std::ofstream file(statsFile);
		if (file) {
			file.setf(std::ios::fixed);
			file.precision(dbl::max_digits10);
			if (!stats.empty()) {
				for (int i = 0; i < stats[0].size(); ++i, file << div) {
					file << stats[0][i].x;
				}
				file << endl;

				for (int k = 0; k < stats.size(); ++k) {
					for (int i = 0; i < stats[k].size(); ++i, file << div) {
						file << stats[k][i].y;
					}
					file << endl;
				}
			}

			file.close();
			std::cout << "Stats saved into " << statsFile << std::endl;
		} else {
			std::cout << "Error: could not save stats into " << statsFile << std::endl;
		}
	}
}