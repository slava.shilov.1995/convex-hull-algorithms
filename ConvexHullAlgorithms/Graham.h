#pragma once
#include "GlobalData.h"

#include <vector>

namespace ghm {
	std::vector<int> Graham(std::vector<glob::Point>& data);
	std::vector<int> GrahamPartial(std::vector<glob::Point>& data, int begin, int end);
}