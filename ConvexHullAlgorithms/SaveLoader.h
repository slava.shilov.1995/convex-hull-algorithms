#pragma once
#include "GlobalData.h"

namespace sl {
	void Save();
	bool Load();
	void SaveStats(std::vector<std::vector<glob::Point>> stats);
}
