#pragma once

#include <vector>
#include <string>

namespace glob {

	struct Point {
		double x;
		double y;
		int id;
		double cmpValue;

		Point(double x, double y, int id) : x(x), y(y), id(id) {}
		Point() {}

		bool operator<(Point& r) {
			return x < r.x || ((x == r.x) && (y > r.y));
		}

		bool operator<<(Point& r) {
			return cmpValue < r.cmpValue || ((cmpValue == r.cmpValue) && ((y > r.y) || (x < r.x)));
		}

		bool operator==(Point& r) {
			return id == r.id;
		}
	};

	enum SortType { StdSort, Qsort, ThreadedQsort, MaxHeap, Introsort };

	extern int N;
	extern std::vector<Point> data;
	extern std::vector<Point> p;//copy of data for executing CH algorithms but not tracking time on data copying 
	extern std::vector<int> ch;
	extern int lastAlgorithm;//0 - none, 1 - Graham, 2 - Jarvis, 3 - Kirkpatrick, 4 - Chan

	extern SortType sortType;
	extern std::vector<std::string> sortTypeTitle;

	void ResetData();
	void ToGlobalIndex(std::vector<int>& vec);
}