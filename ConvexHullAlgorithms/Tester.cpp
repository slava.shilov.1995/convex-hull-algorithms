#include "Tester.h"

#include "Timer.h"
#include "Generator.h"
#include "Graham.h"
#include "Jarvis.h"
#include "Kirkpatrick.h"
#include "Chan.h"

#include <iomanip>
#include <iostream>

namespace tst {

	std::function<std::vector<int>(std::vector<glob::Point>&)> GetCurAlgorithm() {
		switch (glob::lastAlgorithm) {
		case 1: return ghm::Graham; break;
		case 2: return jrv::Jarvis; break;
		case 3: return krk::Kirkpatrick; break;
		case 4: return chn::Chan; break;
		default: return ghm::Graham; break;
		}
	}

	std::vector<std::vector<double>> SortVsSort(glob::SortType sort1, glob::SortType sort2, std::function<std::vector<int>(std::vector<glob::Point>&)> algorithm, std::vector<int> ns, int numExperiments) {
		std::vector<std::vector<double>> dif(4);
		std::cout << std::endl << std::setw(18) << glob::sortTypeTitle[sort1] << " VS " << glob::sortTypeTitle[sort2] << std::endl;
		for (int i = 0; i < 4; ++i) {
			dif[i] = std::vector<double>(ns.size());
			gen::SetDistributionType(gen::DistributionType(i));
			std::cout << gen::distributionTypeTitle[gen::distributionType] << std::endl;
			for (int n_i = 0; n_i < ns.size(); ++n_i) {
				glob::N = ns[n_i];
				double time1 = 0;
				double time2 = 0;
				for (int i = 0; i < numExperiments; ++i) {
					gen::GenerateData(glob::data, false);
					glob::ResetData();
					glob::sortType = glob::SortType(sort1);
					timer::Tic();
					algorithm(glob::p);
					time1 += timer::Toc();
					glob::ResetData();
					glob::sortType = glob::SortType(sort2);
					timer::Tic();
					algorithm(glob::p);
					time2 += timer::Toc();
				}
				dif[i][n_i] = (time2 - time1) / numExperiments;
				
				std::cout << std::setw(10) << ns[n_i] << ": " << std::setw(8) << time1 / numExperiments << std::setw(12) << time2 / numExperiments << " : " << std::setw(10) << dif[i][n_i] / ((time1 / numExperiments) / 100.) << "%" << std::endl;
			}
		}
		
		glob::ch.clear();

		std::cout << "Test done" << std::endl;

		return dif;
	}

	std::vector<std::vector<glob::Point>> AlgorithmWithN(std::function<std::vector<int>(std::vector<glob::Point>&)> algorithm, std::vector<int> ns, int numExperiments) {
		std::cout << "Test with various N" << std::endl;

		std::vector<glob::Point> times(ns.size());
		for (int n_i = 0; n_i < ns.size(); ++n_i) {
			glob::N = ns[n_i];
			double time = 0;
			
			for (int i = 0; i < numExperiments; ++i) {
				gen::GenerateData(glob::data, false);
				glob::ResetData();
				timer::Tic();
				algorithm(glob::p);
				time += timer::Toc();
			}
			
			times[n_i] = glob::Point(ns[n_i], time / numExperiments, 0);
			//times[n_i] = glob::Point(ns[n_i], time, 0);

			std::cout << std::setw(10) << ns[n_i] << ": " << std::setw(8) << times[n_i].y << std::endl;
		}
		std::cout << "Test done" << std::endl;
		return { times };
	}

	std::vector<std::vector<glob::Point>> AllAlgorithmWithN(std::vector<int> ns, int numExperiments) {
		std::cout << "Test all algorithms with various N" << std::endl;

		std::vector<std::vector<glob::Point>> stats(4);

		switch (gen::distributionType) {
		case gen::DistributionType::Edge:
			std::cout << "Graham" << std::endl;
			stats[0] = AlgorithmWithN(ghm::Graham, ns, numExperiments)[0];
			std::cout << "Jarvis" << std::endl;
			stats[1] = AlgorithmWithN(jrv::Jarvis, ns, numExperiments)[0];
			std::cout << "Kirkpatrick" << std::endl;
			stats[2] = AlgorithmWithN(krk::Kirkpatrick, ns, numExperiments)[0];
			std::cout << "Chan" << std::endl;
			stats[3] = AlgorithmWithN(chn::Chan, ns, numExperiments)[0];
			break;
		default:
			std::cout << "Graham" << std::endl;
			stats[0] = AlgorithmWithN(ghm::Graham, ns, numExperiments)[0];
			std::cout << "Jarvis exprectation time exceeds pationable one" << std::endl;
			stats[1] = {};
			for (auto n : ns) {
				stats[1].emplace_back(n, 0.2 * n * log(n), 0);
			}
			std::cout << "Kirkpatrick" << std::endl;
			stats[2] = AlgorithmWithN(krk::Kirkpatrick, ns, numExperiments * 10)[0];
			std::cout << "Chan" << std::endl;
			stats[3] = AlgorithmWithN(chn::Chan, ns, numExperiments)[0];
			break;
		}
		
		std::cout << "Test done" << std::endl;
		return stats;
	}

}