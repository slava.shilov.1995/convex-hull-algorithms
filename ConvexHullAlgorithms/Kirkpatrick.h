#pragma once

#include "GlobalData.h"

namespace krk {
	std::vector<int> Kirkpatrick(std::vector<glob::Point>& points);
}