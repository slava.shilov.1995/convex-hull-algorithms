#pragma once
#include "Visualisation.h"
#include "GlobalData.h"
#include "Generator.h"
#include "SaveLoader.h"
#include "Timer.h"
#include "Tester.h"
#include "Graham.h"
#include "Jarvis.h"
#include "Kirkpatrick.h"
#include "Chan.h"

#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>

namespace vis {

	bool showPoints = true;
	bool showIndices = false;
	bool oriented = true;
	bool showStats = true;

	void TrackAlgorithm(std::vector<int>(*algorithm)(std::vector<glob::Point>&)) {
		timer::Tic();
		glob::ch = algorithm(glob::p);
		timer::Toc();
		std::cout << "Time: " << timer::LastTime() << std::endl;
	}

	//Restart generating points set and calculating CH
	void Reroll(bool withNewData) {
		glob::ch.clear();
		if (withNewData) {
			gen::GenerateData(glob::data, true);
		}
		glob::ResetData();

		switch (glob::lastAlgorithm) {
		case 1: TrackAlgorithm(ghm::Graham); break;
		case 2: TrackAlgorithm(jrv::Jarvis); break;
		case 3: TrackAlgorithm(krk::Kirkpatrick); break;
		case 4: TrackAlgorithm(chn::Chan); break;
		}

		showPoints = true;
	}

	void RunVis() {
		sf::RenderWindow window(sf::VideoMode(500, 500), "Convex hull visualisation!");

		sf::CircleShape point(2.f);
		point.setFillColor(sf::Color::Red);
		point.setOrigin(point.getRadius(), point.getRadius());

		sf::CircleShape chPoint(6.f);
		chPoint.setFillColor(sf::Color::Red);
		chPoint.setOrigin(chPoint.getRadius(), chPoint.getRadius());

		sf::CircleShape statPoint(8.f);
		statPoint.setFillColor(sf::Color::Green);
		statPoint.setOrigin(statPoint.getRadius(), statPoint.getRadius());

		sf::Text text;
		text.setFillColor(sf::Color::White);
		text.setCharacterSize(16);
		sf::Font font;
		font.loadFromFile("Arial.ttf");
		text.setFont(font);
		text.setOrigin(0, 8);



		std::vector<std::vector<glob::Point>> stats;

		Reroll(true);

		while (window.isOpen()) {
			//keyboard controll
			sf::Event event;
			while (window.pollEvent(event)) {
				if (event.type == sf::Event::KeyPressed) {
					if (event.key.code == sf::Keyboard::Escape) {
						window.close();
					} else if (event.key.code == sf::Keyboard::Space) {
						Reroll(true);

					} else if (event.key.code == sf::Keyboard::Q) {
						gen::SetDistributionType(gen::DistributionType::Uniform);
						Reroll(true);
					} else if (event.key.code == sf::Keyboard::E) {
						gen::SetDistributionType(gen::DistributionType::Edge);
						Reroll(true);
					} else if (event.key.code == sf::Keyboard::W) {
						gen::SetDistributionType(gen::DistributionType::Circle);
						Reroll(true);
					} else if (event.key.code == sf::Keyboard::R) {
						gen::SetDistributionType(gen::DistributionType::Ring);
						Reroll(true);
					} else if (event.key.code == sf::Keyboard::T) {
						if (sl::Load()) {
							std::cout << "Loaded from file" << std::endl;
							Reroll(false);
						}

					} else if (event.key.code == sf::Keyboard::P) {
						sl::Save();

					} else if (event.key.code == sf::Keyboard::Tilde) {
						glob::ch.clear();
						glob::lastAlgorithm = 0;
					} else if (event.key.code == sf::Keyboard::Num1) {
						std::cout << "Graham algorithm" << std::endl;
						glob::lastAlgorithm = 1;
						Reroll(false);
					} else if (event.key.code == sf::Keyboard::Num2) {
						std::cout << "Jarvis algorithm" << std::endl;
						glob::lastAlgorithm = 2;
						Reroll(false);
					} else if (event.key.code == sf::Keyboard::Num3) {
						std::cout << "Kirkpatrick algorithm" << std::endl;
						glob::lastAlgorithm = 3;
						Reroll(false);
					} else if (event.key.code == sf::Keyboard::Num4) {
						std::cout << "Chan algorithm" << std::endl;
						glob::lastAlgorithm = 4;
						Reroll(false);

					} else if (event.key.code == sf::Keyboard::Num5) {//test area
						std::cout << "TEST CODE" << std::endl;
						stats = std::vector<std::vector<glob::Point>>(2);
						auto v = gen::LinspaceInt(1000, 1000000, 1000);
						for (auto vi : v) {
							std::vector<int> vv(vi);
							for (int i = 0; i < vv.size(); ++i) {
								vv[i] = (55 + i * 17) % 100;
							}
							timer::Tic();
							std::nth_element(vv.begin(), vv.begin() + vv.size() / 2, vv.end());
							stats[0].emplace_back(vi, timer::Toc() , 0);
							stats[1].emplace_back(vi, vi, 0);
						}

					} else if (event.key.code == sf::Keyboard::A) {
						glob::sortType = glob::SortType::StdSort;
						std::cout << "Sorting: " << glob::sortTypeTitle[glob::sortType] << std::endl;
						Reroll(false);
					} else if (event.key.code == sf::Keyboard::S) {
						glob::sortType = glob::SortType::Qsort;
						std::cout << "Sorting: " << glob::sortTypeTitle[glob::sortType] << std::endl;
						Reroll(false);
					} else if (event.key.code == sf::Keyboard::D) {
						glob::sortType = glob::SortType::ThreadedQsort;
						std::cout << "Sorting: " << glob::sortTypeTitle[glob::sortType] << std::endl;
						Reroll(false);
					} else if (event.key.code == sf::Keyboard::F) {
						glob::sortType = glob::SortType::MaxHeap;
						std::cout << "Sorting: " << glob::sortTypeTitle[glob::sortType] << std::endl;
						Reroll(false);
					} else if (event.key.code == sf::Keyboard::G) {
						glob::sortType = glob::SortType::Introsort;
						std::cout << "Sorting: " << glob::sortTypeTitle[glob::sortType] << std::endl;
						Reroll(false);

					} else if (event.key.code == sf::Keyboard::V) {
						tst::SortVsSort(glob::SortType::StdSort, glob::SortType::Introsort, tst::GetCurAlgorithm(), { 100000 }, 100);
						glob::ch.clear();
					} else if (event.key.code == sf::Keyboard::N) {
						stats = tst::AlgorithmWithN(tst::GetCurAlgorithm(), gen::LinspaceInt(100000, 1000000, 10), 100);
						showPoints = false;
						glob::ch.clear();
					} else if (event.key.code == sf::Keyboard::M) {
						//stats = tst::AllAlgorithmWithN(gen::LinspaceInt(5000, 30000, 50), 20);
						stats = tst::AllAlgorithmWithN(gen::LinspaceInt(5000, 50000, 50), 10);
						sl::SaveStats(stats);
						showPoints = false;
						glob::ch.clear();
					} else if (event.key.code == sf::Keyboard::I) {
						showIndices = showIndices ? false : true;
					} else if (event.key.code == sf::Keyboard::O) {
						oriented = oriented ? false : true;
						std::cout << "oriented: " << oriented << std::endl;
					} else if (event.key.code == sf::Keyboard::U) {
						showStats = showStats ? false : true;
						std::cout << "show stats: " << showStats << std::endl;
					} else if (event.key.code == sf::Keyboard::J) {
						showPoints = showPoints ? false : true;
						std::cout << "show points: " << showPoints << std::endl;
					}

					if (event.key.code == sf::Keyboard::Divide) {
						if (stats.size() > 1) {
							for (int i = 0; i < stats[1].size(); ++i) {
								stats[1][i].y *= 0.9;
							}
						}
					} else if (event.key.code == sf::Keyboard::Multiply) {
						if (stats.size() > 1) {
							for (int i = 0; i < stats[1].size(); ++i) {
								stats[1][i].y *= 1.1;
							}
						}
					}
				} else if (event.type == sf::Event::Closed) {
					window.close();
				}
			}

			//drawing
			window.clear();
			if (showPoints) {
				//drawing CH edges
				for (int i = 0; i < fmin(glob::ch.size(), 1000); ++i) {
					int j = (i == glob::ch.size() - 1) ? 0 : (i + 1);
					double tx1 = glob::p[glob::ch[i]].x;
					double ty1 = glob::p[glob::ch[i]].y;
					double tx2 = glob::p[glob::ch[j]].x;
					double ty2 = glob::p[glob::ch[j]].y;
					double dist = sqrt(pow(tx1 - tx2, 2) + pow(ty1 - ty2, 2)) * 400.0;
					sf::RectangleShape rect(sf::Vector2f(4, dist));
					float angle = atan2(oriented ? ty2 - ty1 : ty1 - ty2, tx1 - tx2) * 180 / 3.14 + 90;
					rect.rotate(angle);
					rect.setPosition(50.0 + tx1 * 400.0, 50.0 + (oriented ? 1 - ty1 : ty1) * 400.0);
					rect.setOutlineThickness(1);
					rect.setFillColor(sf::Color(136, 136, 136));
					rect.setOutlineColor(sf::Color(136, 136, 136));
					rect.setOrigin(2, 0);
					window.draw(rect);
				}
				//drawing CH points
				for (int i = 0; i < fmin(glob::ch.size(), 1000); ++i) {
					chPoint.setPosition(50.0 + glob::p[glob::ch[i]].x * 400.0, 50.0 + (oriented ? 1 - glob::p[glob::ch[i]].y : glob::p[glob::ch[i]].y) * 400.0);
					window.draw(chPoint);
				}
				//drawing CH start point
				if (!glob::ch.empty()) {
					sf::CircleShape chStartPoint(4.0);
					chStartPoint.setOrigin(chStartPoint.getRadius(), chStartPoint.getRadius());
					chStartPoint.setPosition(50.0 + glob::p[glob::ch[0]].x * 400.0, 50.0 + (oriented ? 1 - glob::p[glob::ch[0]].y : glob::p[glob::ch[0]].y) * 400.0);
					chStartPoint.setFillColor(sf::Color::White);
					window.draw(chStartPoint);
				}
				//drawing all points
				for (int i = 0; i < fmin(glob::data.size(), 1000); ++i) {
					point.setPosition(50.0 + glob::data[i].x * 400.0, 50.0 + (oriented ? 1 - glob::data[i].y : glob::data[i].y) * 400.0);
					window.draw(point);
					if (showIndices) {
						std::stringstream ss;
						ss.str("");
						ss << glob::data[i].id;
						text.setString(ss.str());
						text.setPosition(point.getPosition() + point.getOrigin() + point.getOrigin());
						window.draw(text);
					}
				}
				//printing stats
				std::stringstream ss;
				ss.str("");
				ss << "Points: " << glob::N << std::endl << "CH: " << glob::ch.size() << std::endl;
				text.setString(ss.str());
				text.setPosition(0, 8);
				window.draw(text);
			}
			//showing test resuls in curves
			if (showStats && !stats.empty()) {
				double minX = stats[0][0].x, maxX = stats[0][0].x, minY = stats[0][0].y, maxY = stats[0][0].y;
				for (std::vector<glob::Point> line : stats) {
					for (glob::Point point : line) {
						minX = fmin(minX, point.x);
						maxX = fmax(maxX, point.x);
						minY = fmin(minY, point.y);
						maxY = fmax(maxY, point.y);
					}
				}
				std::vector<sf::Color> colors = { sf::Color::Green, sf::Color::Blue, sf::Color::Yellow, sf::Color::Magenta };
				for (int k = 0; k < stats.size(); ++k) {
					if (!stats[k].empty()) {
						for (int i = 0; i < stats[k].size() - 1; ++i) {
							int j = i + 1;
							double tx1 = (stats[k][i].x - minX) / (maxX - minX) * 500;
							double ty1 = (stats[k][i].y - minY) / (maxY - minY) * 500;
							double tx2 = (stats[k][j].x - minX) / (maxX - minX) * 500;
							double ty2 = (stats[k][j].y - minY) / (maxY - minY) * 500;
							double dist = sqrt(pow(tx1 - tx2, 2) + pow(ty1 - ty2, 2));
							sf::RectangleShape rect(sf::Vector2f(4, dist));
							float angle = atan2(oriented ? ty2 - ty1 : ty1 - ty2, tx1 - tx2) * 180 / 3.14 + 90;
							rect.rotate(angle);
							rect.setPosition(tx1, (oriented ? 500 - ty1 : ty1));
							rect.setOutlineThickness(1);
							rect.setFillColor(colors[k]);
							rect.setOutlineColor(colors[k]);
							rect.setOrigin(2, 0);
							window.draw(rect);
						}
						for (int i = 0; i < stats[k].size(); ++i) {

						}
					}
				}
			}

			window.display();
		}

	}
}