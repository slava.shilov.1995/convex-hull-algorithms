#include "Graham.h"
#include "GlobalData.h"
#include "Sortings.h"
#include "Chan.h"

#include <iostream>
#include <thread>
#include <array>
#include <functional>

namespace ghm {

	std::vector<glob::Point>& data = glob::p;
	glob::Point startPoint;

	std::vector<int> Graham(std::vector<glob::Point>& points) {
		//data = points;

		startPoint = data[0];
		for (int i = 1; i < data.size(); ++i) {
			if (data[i].x < startPoint.x) {
				startPoint = data[i];
			} else if (data[i].x == startPoint.x) {
				if (data[i].y > startPoint.y) {
					startPoint = data[i];
				}
			}
		}

		//angle = std::vector<double>(x.size());

		double PI2 = acos(-1.0) * 2;
		for (int i = 0; i < data.size(); ++i) {
			//data[i].cmpValue = atan2(data[i].x - startPoint.x, data[i].y - startPoint.y);
			data[i].cmpValue = -(data[i].y - startPoint.y) / (data[i].x - startPoint.x);
			/*if (data[i].cmpValue < 0) {
				data[i].cmpValue += PI2;
			}*/
		}
		data[startPoint.id].cmpValue = -DBL_MAX;
		std::swap(data[startPoint.id], data[0]);

		srt::SetDataReference(glob::p);
		switch (glob::sortType) {
		case glob::SortType::Qsort:
			srt::Quicksort(1, glob::N - 1);
			//std::cout << "qsort done" << std::endl;
			break;

		case glob::SortType::ThreadedQsort:
			if (glob::N < 1000) {
				srt::Quicksort(1, glob::N - 1);
				//std::cout << "amount of points is not enough: one thread qsort done" << std::endl;
			} else {
				std::thread t1, t2, t3, t4;//4 cores
				int p1 = srt::Partition(1, glob::N - 1);
				if (1 < p1) {
					int p2 = srt::Partition(1, p1);
					t1 = std::thread(srt::Quicksort, 1, p2);
					t2 = std::thread(srt::Quicksort, p2 + 1, p1);
				}
				if (p1 + 1 < glob::N - 1) {
					int p3 = srt::Partition(p1 + 1, glob::N - 1);
					t3 = std::thread(srt::Quicksort, p1 + 1, p3);
					t4 = std::thread(srt::Quicksort, p3 + 1, glob::N - 1);
				}
				t1.join();
				t2.join();
				t3.join();
				t4.join();
				//std::cout << "threaded qsort done" << std::endl;
			}
			
			break;

		case glob::SortType::MaxHeap:
			srt::MaxHeapSort(1, glob::N - 1);
			//std::cout << "max heap sort done" << std::endl;
			break;

		case glob::SortType::Introsort:
			srt::Introsort(1, glob::N - 1);
			//std::cout << "introsort done" << std::endl;
			break;

		case glob::SortType::StdSort:
		default:
			//std::sort(data.begin(), data.end(), [](glob::Point& a, glob::Point& b) {return a.cmpValue < b.cmpValue || ((a.cmpValue == b.cmpValue) && ((a.y > b.y) || (a.x < b.x))); });
			std::sort(data.begin(), data.end(), [](glob::Point& a, glob::Point& b) { return a << b; });
			//std::cout << "std::sort done" << std::endl;
			break;
		}

		int count = 0;
		double c;
		std::vector<int> ch;
		for (int i = 0; i < data.size(); ++i) {
			if (count < 2) {
				ch.emplace_back(i);
				++count;
			} else {
				c = (data[ch[count - 1]].x - data[ch[count - 2]].x) * (data[i].y - data[ch[count - 2]].y) - (data[ch[count - 1]].y - data[ch[count - 2]].y) * (data[i].x - data[ch[count - 2]].x);
				if (c >= 0) {
					ch.pop_back();
					--count;
					--i;
				} else {
					ch.emplace_back(i);
					++count;
				}
			}
		}

		/*for (int i = 0; i < count; ++i) {
			ch[i] = data[ch[i]].id;
		}*/

		return ch;
	}

	std::vector<int> GrahamPartial(std::vector<glob::Point>& points, int begin, int end) {
		int size = end - begin + 1;
		data = points;

		int bestPos = begin;
		startPoint = data[begin];
		for (int i = begin + 1; i <= end; ++i) {
			if (data[i].x < startPoint.x) {
				startPoint = data[i];
				bestPos = i;
			} else if (data[i].x == startPoint.x) {
				if (data[i].y > startPoint.y) {
					startPoint = data[i];
					bestPos = i;
				}
			}
		}

		for (int i = begin; i <= end; ++i) {
			data[i].cmpValue = -(data[i].y - startPoint.y) / (data[i].x - startPoint.x);
		}
		data[bestPos].cmpValue = -DBL_MAX;
		std::swap(data[bestPos], data[begin]);

		srt::SetDataReference(std::ref(data));
		switch (glob::sortType) {
		case glob::SortType::Qsort:
			srt::Quicksort(begin + 1, end);
			break;
		case glob::SortType::ThreadedQsort:
			if (glob::N < 1000) {
				srt::Quicksort(begin + 1, end);
			} else {
				std::thread t1, t2, t3, t4;//4 cores
				int p1 = srt::Partition(begin + 1, end);
				if (begin + 1 < p1) {
					int p2 = srt::Partition(begin + 1, p1);
					t1 = std::thread(srt::Quicksort, begin + 1, p2);
					t2 = std::thread(srt::Quicksort, p2 + 1, p1);
				}
				if (p1 + 1 < end) {
					int p3 = srt::Partition(p1 + 1, end);
					t3 = std::thread(srt::Quicksort, p1 + 1, p3);
					t4 = std::thread(srt::Quicksort, p3 + 1, end);
				}
				t1.join();
				t2.join();
				t3.join();
				t4.join();
			}
			break;
		case glob::SortType::MaxHeap:
			srt::MaxHeapSort(begin + 1, end);
			break;

		case glob::SortType::Introsort:
			srt::Introsort(begin + 1, end);
			break;

		case glob::SortType::StdSort:
		default:
			std::sort(data.begin() + begin, data.begin() + end + 1, [](glob::Point& a, glob::Point& b) { return a << b; });
			break;
		}

		int count = 0;
		double c;
		std::vector<int> ch;
		for (int i = begin; i <= end; ++i) {
			if (count < 2) {
				ch.emplace_back(i);
				++count;
			} else {
				c = (data[ch[count - 1]].x - data[ch[count - 2]].x) * (data[i].y - data[ch[count - 2]].y) - (data[ch[count - 1]].y - data[ch[count - 2]].y) * (data[i].x - data[ch[count - 2]].x);
				if (c >= 0) {
					ch.pop_back();
					--count;
					--i;
				} else {
					ch.emplace_back(i);
					++count;
				}
			}
		}

		return ch;
	}

}