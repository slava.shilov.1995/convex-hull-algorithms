#include "Chan.h"
#include "Graham.h"

#include <iostream>

using glob::p;

namespace chn {

	glob::Point lastPoint;
	glob::Point nextPoint;

	std::vector<int> lps;

	glob::Point MaxGlobalAnglePoint(std::vector<int>& ch, int j) {
		/*std::cout << "in ";
		for (int i = 0; i < ch.size(); ++i) {
			std::cout << p[ch[i]].id << " ";
		}*/

		for (int i = lps[j]; i < ch.size() - 1; ++i) {
			if ((p[ch[i + 1]].x - lastPoint.x) * (p[ch[i]].y - lastPoint.y) - (p[ch[i + 1]].y - lastPoint.y) * (p[ch[i]].x - lastPoint.x) <= 0 || p[ch[i]] == lastPoint) {
				++lps[j];
			} else {
				//std::cout << " out " << p[ch[lps[j]]].id << std::endl;
				return p[ch[lps[j]]];
			}
		}
		if ((p[ch[0]].x - lastPoint.x) * (p[ch[ch.size() - 1]].y - lastPoint.y) - (p[ch[0]].y - lastPoint.y) * (p[ch[ch.size() - 1]].x - lastPoint.x) <= 0 || p[ch[ch.size() - 1]] == lastPoint) {
			lps[j] = 0;
		}
		//std::cout << " out " << p[ch[lps[j]]].id << std::endl;
		return p[ch[lps[j]]];
	}

	glob::Point BinarySearchPoint(std::vector<int>& ch, int j) {
		/*std::cout << "in ";
		for (int i = 0; i < ch.size(); ++i) {
			std::cout << p[ch[i]].id << " ";
		}*/
		if (lps[j] >= ch.size()) {
			return p[ch[0]];
		}
		int low = lps[j];
		int high = ch.size() - 1;
		int bestPoint = (high + 1) / 2;
		int cur;
		double slope;
		while (low <= high) {
			cur = (low + high + 1) / 2;
			//std::cout << " c=" << cur << " " << p[ch[cur]].id;
			
			if ((p[ch[cur]].y - lastPoint.y) * (p[ch[bestPoint]].x - lastPoint.x) - (p[ch[cur]].x - lastPoint.x) * (p[ch[bestPoint]].y - lastPoint.y) > 0) {
				bestPoint = cur;
			}
			if (cur == 0) {
				slope = +DBL_MAX;
			} else {
				slope = (p[ch[cur]].y - p[ch[cur - 1]].y) * (p[ch[cur]].x - lastPoint.x) - (p[ch[cur]].x - p[ch[cur - 1]].x) * (p[ch[cur]].y - lastPoint.y);
			}
			
			if (slope > 0) {
				low = cur + 1;
				/*if (low >= ch.size()) {
					lps[j] = low;
					return p[ch[0]];
				}*/
			} else if (slope < 0) {
				high = cur - 1;
			} else {
				//bestPoint = cur+1;
				low = cur + 1;
			}
		}
		if (p[ch[bestPoint]] == lastPoint) {
			++bestPoint;
			if (bestPoint >= ch.size()) {
				bestPoint = 0;
			}
		}
		//std::cout << " out " << p[ch[bestPoint]].id << "   slope=" << slope << std::endl;
		lps[j] = bestPoint;
		return p[ch[bestPoint]];
	}

	std::vector<int> Hull(int m) {
		std::vector<glob::Point> nextData;
		int r = std::ceil((double)p.size() / m);
		//std::cout << "r=" << r << std::endl;
		std::vector<std::vector<int>> chs(r);
		lps = std::vector<int>(r);
		for (int i = 0; i < r; ++i) {
			//std::cout << "ghm: " << m * i << " " << fmin(m * (i + 1) - 1, p.size() - 1) << std::endl;
			chs[i] = ghm::GrahamPartial(p, m * i, fmin(m * (i + 1) - 1, p.size() - 1));
			chs[i].push_back(chs[i].front());
			/*for (int j = 0; j < chs[i].size(); ++j) {
				std::cout << p[chs[i][j]].id << " ";
			}
			std::cout << std::endl;*/
			for (int j = 0; j < chs[i].size(); ++j) {
				nextData.emplace_back(p[chs[i][j]]);
			}
			lps[i] = 0;
		}

		glob::Point startPoint = p[chs[0][0]];
		glob::Point point;
		for (int i = 0; i < r; ++i) {
			point = p[chs[i][0]];
			if (point.x < startPoint.x || (point.x == startPoint.x && point.y > startPoint.y)) {
				startPoint = point;
			}
		}
		std::vector<int> ch;
		ch.emplace_back(startPoint.id);
		lastPoint = startPoint;
		//std::cout << "startPoint " << startPoint.id << std::endl;
		for (int i = 0; i < m; ++i) {
			nextPoint = MaxGlobalAnglePoint(chs[0], 0);
			for (int j = 1; j < r; ++j) {
				point = MaxGlobalAnglePoint(chs[j], j);
				double c = (point.y - lastPoint.y) * (nextPoint.x - lastPoint.x) - (point.x - lastPoint.x) * (nextPoint.y - lastPoint.y);
				if (c >= 0) {
					if (c == 0) {
						if ((point.x - nextPoint.x) * (nextPoint.x - lastPoint.x) + (point.y - nextPoint.y) * (nextPoint.y - lastPoint.y) > 0) {
							nextPoint = point;
						}
					} else {
						nextPoint = point;
					}
;				}
			}
			lastPoint = nextPoint;
			if (lastPoint == startPoint) {
				return ch;
			}
			ch.emplace_back(lastPoint.id);
			//std::cout << "ch << " << nextPoint.id << std::endl;
		}
		ch.clear();
		//p = nextData;
		return ch;
	}

	std::vector<int> Chan(std::vector<glob::Point>& points) {
		p = points;
		std::vector<int> ch;
		int m = 8;
		while (true) {
			//std::cout << "m=" << m << std::endl;
			ch = Hull(m);
			if (ch.empty()) {
				if (m > glob::data.size()) {
					break;
				}
				m *= m;
			} else {
				break;
			}
		}

		/*for (int i = 0; i < ch.size(); ++i) {
			std::cout << ch[i] << " ";
		}
		std::cout << std::endl;*/

		glob::ResetData();

		return ch;
	}
}