#pragma once

#include "Visualisation.h"

#include <vector>
#include <iostream>

#include <algorithm>

bool comp(int a, int b)
{
	return (a < b);
}

int main() {
	std::vector<int> v{ 5, 6, 4, 3, 2, 6, 7, 9, 3, 6, 4, 7, 2, 2, 9, 9 };

	std::nth_element(v.begin(), v.begin() + v.size() / 2, v.end());
	std::cout << "The median is " << v[v.size() / 2] << '\n';

	std::nth_element(v.begin(), v.begin() + 1, v.end(), std::greater<int>());
	std::cout << "The second largest element is " << v[1] << '\n';

	vis::RunVis();
	return 0;
}