#pragma once

#include "GlobalData.h"

namespace chn {
	extern std::vector<glob::Point> data;

	std::vector<int> Chan(std::vector<glob::Point>& points);
}