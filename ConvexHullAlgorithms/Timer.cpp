#pragma once 
#include "Timer.h"

#include <chrono>

namespace timer {
	std::chrono::high_resolution_clock::time_point start;
	std::chrono::high_resolution_clock::time_point end;
	double last;

	void Tic() {
		start = std::chrono::high_resolution_clock::now();
	}

	double Toc() {
		end = std::chrono::high_resolution_clock::now();
		last = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count() / 1000000000.0;
		return last;
	}

	double LastTime() {
		return last;
	}
}