#pragma once


#include "GlobalData.h"

namespace glob {
	int N = 10000000;
	std::vector<Point> data(N);
	std::vector<Point> p = data;
	std::vector<int> ch;
	int lastAlgorithm = 4;

	SortType sortType = SortType::Introsort;
	std::vector<std::string> sortTypeTitle = { "StdSort", "Qsort", "ThreadedQsort", "MaxHeap", "Introsort" };

	void ResetData() {
		p = data;
	}

	void ToGlobalIndex(std::vector<int>& vec) {
		for (int i = 0; i < vec.size(); vec[i] = p[vec[i++]].id);
	}
}