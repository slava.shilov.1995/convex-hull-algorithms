#pragma once
#include "Generator.h"
#include "GlobalData.h"

#include <iostream>
#include <random>

namespace gen {

	std::vector<std::string> distributionTypeTitle = {"Square", "Circle", "Frame", "Ring"};

	DistributionType distributionType = DistributionType::Edge;
	std::random_device device;

	void GenerateData(std::vector<glob::Point>& data, bool verbose) {
		if (glob::N != data.size()) {
			data.resize(glob::N);
		}
		int length = data.size();
		std::uniform_real_distribution<double> realDistribution(0.0, 1.0);
		if (distributionType == Uniform) {
			for (int i = 0; i < length; ++i) {
				data[i] = glob::Point(realDistribution(device), realDistribution(device), i);
			}
		} else if (distributionType == Edge) {
			std::uniform_int_distribution<int> intDistribution(0, 3);
			for (int i = 0; i < length; ++i) {
				switch (intDistribution(device)) {
				case 0:
					data[i] = glob::Point(realDistribution(device), 0, i);
					break;
				case 1:
					data[i] = glob::Point(1, realDistribution(device), i);
					break;
				case 2:
					data[i] = glob::Point(realDistribution(device), 1, i);
					break;
				case 3:
					data[i] = glob::Point(0, realDistribution(device), i);
					break;
				}
			}
		} else if (distributionType == Circle) {
			for (int i = 0; i < length; ++i) {
				data[i] = glob::Point(realDistribution(device), realDistribution(device), i);
				if (pow(data[i].x - 0.5, 2) + pow(data[i].y - 0.5, 2) > 0.25) {
					--i;
				}
			}
		} else if (distributionType == Ring) {
			std::uniform_real_distribution<double> angleDistribution(0.0, acos(-1.0) * 2);//<- [0, 2PI]
			double angle;
			for (int i = 0; i < length; ++i) {
				angle = angleDistribution(device);
				data[i] = glob::Point(0.5 + cos(angle) * 0.5, 0.5 + sin(angle) * 0.5, i);
			}
		}
		if (verbose) {
			std::cout << "Generated new data: " << distributionTypeTitle[distributionType] << " distribution" << std::endl;
		}
		
	}

	void SetDistributionType(gen::DistributionType type) {
		distributionType = type;
	}

	std::vector<int> LinspaceInt(int start, int end, int steps) {
		std::vector<int> v(steps+1);
		for (int i = 0; i < v.size(); ++i) {
			v[i] = start + (end - start) / steps * i;
		}
		return v;
	}

}