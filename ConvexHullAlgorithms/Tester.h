#pragma once
#include "GlobalData.h"
#include <functional>

namespace tst {
	std::function<std::vector<int>(std::vector<glob::Point>&)> GetCurAlgorithm();
	std::vector<std::vector<double>> SortVsSort(glob::SortType sort1, glob::SortType sort2, std::function<std::vector<int>(std::vector<glob::Point>&)> algorithm, std::vector<int> ns, int numExperiments);
	std::vector<std::vector<glob::Point>> AlgorithmWithN(std::function<std::vector<int>(std::vector<glob::Point>&)> algorithm, std::vector<int> ns, int numExperiments);
	std::vector<std::vector<glob::Point>> AllAlgorithmWithN(std::vector<int> ns, int numExperiments);
}