#pragma once

namespace timer {
	void Tic();
	double Toc();
	double LastTime();
}